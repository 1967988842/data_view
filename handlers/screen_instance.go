package handlers

import (
	"data_view/core"
	"data_view/middleware"
	"data_view/models"
	"github.com/kataras/iris/v12"
)

func GetScreenInstanceList(context iris.Context) {
	var (
		err           error
		pagingRequest *core.PagingRequest
		list          []*models.ScreenInstance
		count         int64
	)
	if pagingRequest, err = core.NewPagingRequest(context); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 请求参数错误，不能正确格式化
		_, _ = context.JSON(ApiResourceError(core.RequestBodyError))
		return
	}
	// 查询数据库
	if list, count, err = models.GetScreenInstancePage(pagingRequest); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 请求参数错误，不能正确格式化
		_, _ = context.JSON(ApiResourceError(err.Error()))
		return
	}
	context.StatusCode(iris.StatusOK)
	_, _ = context.JSON(ApiResourceSuccess(map[string]interface{}{"list": list, "count": count}))
	return
}

func GetScreenInstance(context iris.Context) {
	var (
		err            error
		id             uint64
		screenInstance *models.ScreenInstanceParams
	)
	// 获取ID
	if id, err = context.Params().GetUint64("id"); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 请求参数错误，不能正确格式化
		_, _ = context.JSON(ApiResourceError(core.RequestBodyError))
		return
	}
	// 查询数据库
	if screenInstance, err = models.GetScreenInstanceById(id); err != nil {
		// 程序内部错误
		context.StatusCode(iris.StatusInternalServerError)
		// 查询数据库错误
		_, _ = context.JSON(ApiResourceError(err.Error()))
		return
	}
	context.StatusCode(iris.StatusOK)
	_, _ = context.JSON(ApiResourceSuccess(screenInstance))
	return
}

// todo: 此方法暂时不好用，需要修改从body中获取数据
func DeleteScreenInstance(context iris.Context) {
	id, err := context.Params().GetUint64("id")
	if err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 请求参数错误，不能正确格式化
		_, _ = context.JSON(ApiResourceError(core.RequestBodyError))
		return
	}
	// 更新数据库
	if err := models.DeleteScreenInstanceById(id); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusInternalServerError)
		// 更新数据库错误
		_, _ = context.JSON(ApiResourceError(err.Error()))
		return
	}
	context.StatusCode(iris.StatusOK)
	_, _ = context.JSON(ApiResourceSuccess(nil))
	return
}

func SaveScreenInstance(context iris.Context) {
	// 格式化输入信息
	screenInstanceJson := new(models.ScreenInstanceJson)
	if err := context.ReadJSON(&screenInstanceJson); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 请求参数错误，不能格式化成结构体
		_, _ = context.JSON(ApiResourceError(core.RequestBodyError))
		return
	}
	// 校验结构体参数是否符合规则
	if err := validate.Struct(screenInstanceJson); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 参数不符合规范
		_, _ = context.JSON(ApiResourceError(validatorErrorData(err)))
		return
	}
	editUser := middleware.GetUser()
	instanceId, err := models.SaveScreenInstance(screenInstanceJson, editUser)
	if err != nil {
		// 程序内部错误
		context.StatusCode(iris.StatusInternalServerError)
		// 保存数据库错误
		_, _ = context.JSON(ApiResourceError(err.Error()))
		return
	}
	context.StatusCode(iris.StatusOK)
	_, _ = context.JSON(ApiResourceSuccess(instanceId))
	return
}

func UpdateScreenInstance(context iris.Context) {
	// 格式化输入信息
	screenInstanceJson := new(models.ScreenInstanceJson)
	if err := context.ReadJSON(&screenInstanceJson); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 请求参数错误，不能格式化成结构体
		_, _ = context.JSON(ApiResourceError(core.RequestBodyError))
		return
	}
	// 校验结构体参数是否符合规则
	if err := validate.Struct(screenInstanceJson); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 参数不符合规范
		_, _ = context.JSON(ApiResourceError(validatorErrorData(err)))
		return
	}
	editUser := middleware.GetUser()
	if err := models.UpdateScreenInstance(screenInstanceJson, editUser); err != nil {
		// 程序内部错误
		context.StatusCode(iris.StatusInternalServerError)
		// 保存数据库错误
		_, _ = context.JSON(ApiResourceError(err.Error()))
		return
	}
	context.StatusCode(iris.StatusOK)
	_, _ = context.JSON(ApiResourceSuccess(nil))
	return
}
