package handlers

import (
	"data_view/models"
	"data_view/models/charts"
)

type ApiResponseJson struct {
	Status int         `json:"status"`
	Msg    interface{} `json:"msg"`
	Data   interface{} `json:"data"`
}

func ApiResource(status int, objects interface{}, msg string) (apiJson *ApiResponseJson) {
	apiJson = &ApiResponseJson{Status: status, Data: objects, Msg: msg}
	return
}

func ApiResourceSuccess(objects interface{}) (apiJson *ApiResponseJson) {
	apiJson = &ApiResponseJson{Status: 200, Data: objects, Msg: "success"}
	return
}

func ApiResourceError(msg string) (apiJson *ApiResponseJson) {
	apiJson = &ApiResponseJson{Status: 500, Data: nil, Msg: msg}
	return
}

const (
	PlotBubble                  = "plotBubble"
	PlotMap                     = "plotMap"
	LineNormal                  = "lineNormal"
	LineStacking                = "lineStacking"
	LineStackingArea            = "lineStackingArea"
	HistogramGradient           = "histogramGradient"
	HistogramGradientHorizontal = "histogramGradientHorizontal"
	HistogramStacking           = "histogramStacking"
	HistogramComplex            = "histogramComplex"
	HistogramTwoBar             = "histogramTwoBar"
	MapChina                    = "mapChina"
	MapProvince                 = "mapProvince"
	PieNormal                   = "pieNormal"
	PieRing                     = "pieRing"
	PieRings                    = "pieRings"
	Pie2D                       = "pie2D"
	PiePercent                  = "piePercent"
	RadarBasic                  = "radarBasic"
	HeatBasic                   = "heatBasic"
	RelationOne                 = "relationOne"
	RelationTwo                 = "relationTwo"
	RelationThree               = "relationThree"
	RelationFour                = "relationFour"
	RelationFive                = "relationFive"
	WordCloud                   = "wordCloud"
	RotationList                = "rotationList"
	Counter                     = "counter"
	Gauge                       = "gauge"
)

func init() {
	models.ChartDataHandlers[PlotBubble] = charts.PlotBubbleGetDataHandle
	models.ChartDataHandlers[PlotMap] = charts.PlotMapGetDataHandle
	models.ChartDataHandlers[LineNormal] = charts.LineNormalGetDataHandle
	models.ChartDataHandlers[LineStacking] = charts.LineStackingGetDataHandle
	models.ChartDataHandlers[LineStackingArea] = charts.LineStackingAreaGetDataHandle
	models.ChartDataHandlers[HistogramGradient] = charts.HistogramGradientGetDataHandle
	models.ChartDataHandlers[HistogramGradientHorizontal] = charts.HistogramGradientGetDataHandle
	models.ChartDataHandlers[HistogramStacking] = charts.HistogramStackingGetDataHandle
	models.ChartDataHandlers[HistogramComplex] = charts.HistogramComplexGetDataHandle
	models.ChartDataHandlers[HistogramTwoBar] = charts.HistogramTwoBarGetDataHandle
	models.ChartDataHandlers[MapChina] = charts.MapChinaGetDataHandle
	models.ChartDataHandlers[MapProvince] = charts.MapProvinceGetDataHandle
	models.ChartDataHandlers[PieNormal] = charts.PieNormalGetDataHandle
	models.ChartDataHandlers[PieRing] = charts.PieNormalGetDataHandle
	models.ChartDataHandlers[PieRings] = charts.PieRingsGetDataHandle
	models.ChartDataHandlers[Pie2D] = charts.PieNormalGetDataHandle
	models.ChartDataHandlers[PiePercent] = charts.PiePercentGetDataHandle
	models.ChartDataHandlers[RadarBasic] = charts.RadarBasicGetDataHandle
	models.ChartDataHandlers[HeatBasic] = charts.HeatBasicGetDataHandle
	models.ChartDataHandlers[RelationOne] = charts.RelationOneGetDataHandle
	models.ChartDataHandlers[RelationTwo] = charts.RelationTwoGetDataHandle
	models.ChartDataHandlers[RelationThree] = charts.RelationThreeGetDataHandle
	models.ChartDataHandlers[RelationFour] = charts.RelationFourGetDataHandle
	models.ChartDataHandlers[RelationFive] = charts.RelationFiveGetDataHandle
	models.ChartDataHandlers[WordCloud] = charts.PieNormalGetDataHandle
	models.ChartDataHandlers[RotationList] = charts.RotationListGetDataHandle
	models.ChartDataHandlers[Counter] = charts.CounterGetDataHandle
	models.ChartDataHandlers[Gauge] = charts.GaugeGetDataHandle
}
