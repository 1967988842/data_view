package handlers

import (
	"data_view/core"
	"data_view/models"
	"github.com/kataras/iris/v12"
)

func GetChartData(context iris.Context) {
	var (
		err             error
		chartDataParams *core.ChartDataParams
		dataResult      string
	)
	// 格式化输入信息
	if chartDataParams, err = core.NewChartDataRequest(context); err != nil {
		// 错误的请求
		context.StatusCode(iris.StatusBadRequest)
		// 请求参数错误，不能正确格式化
		_, _ = context.JSON(ApiResourceError(core.RequestBodyError))
		return
	}
	// 查询数据库
	if dataResult, err = models.GetChartData(chartDataParams); err != nil {
		// 程序内部错误
		context.StatusCode(iris.StatusInternalServerError)
		// 查询数据库错误
		_, _ = context.JSON(ApiResourceError(err.Error()))
		return
	}
	context.StatusCode(iris.StatusOK)
	_, _ = context.JSON(ApiResourceSuccess(dataResult))
	return
}
