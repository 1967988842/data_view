package core

const (
	DelFlag      = "del_flag"
	IsNotExist   = 0
	IsExist      = 1
	DefaultOrder = "edit_time desc"
	MySQL        = "MySQL"
	Oracle       = "Oracle"
	SQLServer    = "SQLServer"
	DB2          = "DB2"
)

// 错误信息
const (
	StatusNotFound            = "404 Not Found"
	StatusInternalServerError = "请求超时，请重试！"
	RequestBodyError          = "请求参数错误！"
	DataSourceTypeError       = "数据源类型未匹配"
)

const (
	DataBase = "DataBase"
	CSV      = "CSV"
)

const EmptyString = ""

const MaxSize = 5 << 20
