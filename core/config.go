package core

import (
	"fmt"
	"github.com/pelletier/go-toml"
)

var Config *toml.Tree

/**
 * 返回单例实例
 * @method NewEngine
 */
func init() {
	Config, err = toml.LoadFile("config.toml")
	if err != nil {
		fmt.Println("TomlError ", err.Error())
	}
}
