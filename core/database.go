package core

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/pelletier/go-toml"
	"xorm.io/core"
	"xorm.io/xorm"
)

var DB *xorm.Engine

/**
*设置数据库连接
*@param diver string
 */
func init() {
	driver := Config.Get("database.driver").(string)
	configTree := Config.Get(driver).(*toml.Tree)
	userName := configTree.Get("databaseUserName").(string)
	password := configTree.Get("databasePassword").(string)
	databaseName := configTree.Get("databaseName").(string)
	connect := userName + ":" + password + "@/" + databaseName + "?charset=utf8&parseTime=True&loc=Local"
	DB, err = xorm.NewEngine(driver, connect)
	if err != nil {
		panic(fmt.Sprintf("No error should happen when connecting to  database, but got err=%+v", err))
	}
	DB.SetTableMapper(core.SnakeMapper{})
	DB.SetColumnMapper(core.GonicMapper{})
	DB.SetMaxIdleConns(10)
	DB.SetMaxOpenConns(50)
	DB.ShowSQL(true)
	DB.Logger().SetLevel(core.LOG_DEBUG)
}
