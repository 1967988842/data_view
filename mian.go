package main

import (
	"data_view/core"
	"data_view/handlers"
	"data_view/middleware"
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/core/router"
	"github.com/kataras/iris/v12/middleware/logger"
)

func newApp() (api *iris.Application) {
	api = iris.New()
	api.Use(logger.New())

	api.OnErrorCode(iris.StatusNotFound, func(context iris.Context) {
		_, _ = context.JSON(handlers.ApiResource(iris.StatusNotFound, nil, core.StatusNotFound))
	})
	api.OnErrorCode(iris.StatusInternalServerError, func(context iris.Context) {
		_, _ = context.WriteString(core.StatusInternalServerError)
	})

	iris.RegisterOnInterrupt(func() {
		_ = core.DB.Close()
	})

	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"}, //允许通过的主机名称
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS", "OPTION"},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
	})

	v1 := api.Party("/api/v1", crs).AllowMethods(iris.MethodOptions)
	{
		v1.Use(middleware.CheckToken)
		v1.PartyFunc("/data_view", func(dataViewApi router.Party) {
			dataViewApi.PartyFunc("/data_source", func(dataSourceApi router.Party) {
				dataSourceApi.Get("/", handlers.GetDataSourcePage)
				dataSourceApi.Get("/list", handlers.GetDataSourceList)
				dataSourceApi.Get("/{id:uint64}", handlers.GetDataSource)
				dataSourceApi.Delete("/", handlers.DeleteDataSource)
				dataSourceApi.PartyFunc("/test_connection", func(testConnectionApi router.Party) {
					testConnectionApi.Post("/", handlers.TestConnection)
				})
				dataSourceApi.Post("/", handlers.SaveDataSource)
				dataSourceApi.Put("/", handlers.UpdateDataSource)
			})
			dataViewApi.PartyFunc("/screen_instance", func(screenInstanceApi router.Party) {
				screenInstanceApi.Get("/", handlers.GetScreenInstanceList)
				screenInstanceApi.Get("/{id:uint64}", handlers.GetScreenInstance)
				screenInstanceApi.Delete("/", handlers.DeleteScreenInstance)
				screenInstanceApi.Post("/", handlers.SaveScreenInstance)
				screenInstanceApi.Put("/", handlers.UpdateScreenInstance)
			})
			dataViewApi.PartyFunc("/chart_data", func(ChartDataApi router.Party) {
				ChartDataApi.Get("/", handlers.GetChartData)
			})
			dataViewApi.PartyFunc("/image", func(ImageApi router.Party) {
				ImageApi.Get("/list", handlers.GetImageBgList)
				ImageApi.Post("/", iris.LimitRequestBodySize(core.MaxSize), handlers.SaveImage)
				ImageApi.Get("/{id:uint64}", handlers.GetImage)
			})
			dataViewApi.PartyFunc("/chart_build", func(ChartBuildApi router.Party) {
				ChartBuildApi.Get("/{id:uint64}", handlers.CreateChartPkg)
			})
		})
	}
	return
}

func main() {
	app := newApp()
	addr := core.Config.Get("app.addr").(string)
	_ = app.Run(iris.Addr(addr))
}
