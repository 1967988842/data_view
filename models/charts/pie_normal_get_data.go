package charts

import (
	"data_view/core"
	"data_view/models"
	"database/sql"
	"encoding/json"
	"strings"
)

var PieNormalGetDataHandle = &models.ChartDataHandler{RunGetDataFromDB: func(db *sql.DB, chartDataParams *core.ChartDataParams) (string, error) {
	sqlString := chartDataParams.Sql
	rows, err := db.Query(sqlString)
	if err != nil {
		return core.EmptyString, err
	}
	defer rows.Close()
	dataResults, err := PieNormalFormatRows(rows, chartDataParams)
	if err != nil {
		return core.EmptyString, err
	}
	resultString, err := json.Marshal(dataResults)
	if err != nil {
		return core.EmptyString, err
	}
	return string(resultString), nil
}, RunGetDataFromCsv: func(chartDataParams *core.ChartDataParams) (string, error) {
	return "", nil
}}

func PieNormalFormatRows(rows *sql.Rows, chartDataParams *core.ChartDataParams) (*[]interface{}, error) {
	// 普通饼图==环形饼图==2D饼图==词云
	// 图表所需要的字段和数据库中字段的对应关系
	nameField := chartDataParams.Name
	valueField := chartDataParams.Value
	// 返回值列表
	dataResults := make([]interface{}, 0)
	columns, err := rows.Columns()
	if err != nil {
		return &dataResults, err
	}
	values := make([]sql.RawBytes, len(columns))
	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}
	for rows.Next() {
		if err := rows.Scan(scanArgs...); err != nil {
			return &dataResults, err
		}
		dataResult := make(map[string]interface{})
		var value string
		for i, col := range values {
			if col == nil {
				value = "NULL"
			} else {
				value = string(col)
			}
			if strings.EqualFold(columns[i], nameField) {
				dataResult["name"] = value
			}
			if strings.EqualFold(columns[i], valueField) {
				dataResult["value"] = value
			}
		}
		dataResults = append(dataResults, dataResult)
	}
	return &dataResults, nil
}
