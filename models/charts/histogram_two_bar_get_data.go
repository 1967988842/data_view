package charts

import (
	"data_view/core"
	"data_view/models"
	"data_view/utils"
	"database/sql"
	"encoding/json"
	"strings"
)

var HistogramTwoBarGetDataHandle = &models.ChartDataHandler{RunGetDataFromDB: func(db *sql.DB, chartDataParams *core.ChartDataParams) (string, error) {
	sqlString := chartDataParams.Sql
	rows, err := db.Query(sqlString)
	if err != nil {
		return core.EmptyString, err
	}
	defer rows.Close()
	dataResults, err := HistogramTwoBarFormatRows(rows, chartDataParams)
	if err != nil {
		return core.EmptyString, err
	}
	resultString, err := json.Marshal(dataResults)
	if err != nil {
		return core.EmptyString, err
	}
	return string(resultString), nil
}, RunGetDataFromCsv: func(chartDataParams *core.ChartDataParams) (string, error) {
	return "", nil
}}

func HistogramTwoBarFormatRows(rows *sql.Rows, chartDataParams *core.ChartDataParams) (*map[string]interface{}, error) {
	// 图表所需要的字段和数据库中字段的对应关系
	xField := chartDataParams.X
	yField := chartDataParams.Y
	legendField := chartDataParams.Legend

	// 返回值列表
	resultMap := make(map[string]interface{})
	tempResults := make([]map[string]string, 0)
	// 列名
	columns, err := rows.Columns()
	if err != nil {
		return &resultMap, err
	}
	// 读取到的数据库数据切片
	values := make([]sql.RawBytes, len(columns))
	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}
	legendList := make([]string, 0)
	yList := make([]string, 0)
	for rows.Next() {
		if err := rows.Scan(scanArgs...); err != nil {
			return &resultMap, err
		}
		var value string
		// 获取数据初步规范
		tempResultMap := make(map[string]string)
		for i, col := range values {
			if col == nil {
				value = "NULL"
			} else {
				value = string(col)
			}
			if strings.EqualFold(columns[i], xField) {
				tempResultMap[xField] = value
			}
			if strings.EqualFold(columns[i], legendField) {
				tempResultMap[legendField] = value
				legendList = append(legendList, value)
			}
			if strings.EqualFold(columns[i], yField) {
				tempResultMap[yField] = value
				yList = append(yList, value)
			}
		}
		tempResults = append(tempResults, tempResultMap)
	}
	// 规范数据
	valueList := make([]interface{}, 0)
	for _, legend := range utils.Duplicate(legendList) {
		valueMap := make(map[string]interface{})
		xResultList := make([]string, 0)
		for _, tempResult := range tempResults {
			if strings.EqualFold(legend, tempResult[legendField]) {
				xResultList = append(xResultList, tempResult[xField])
			}
		}
		valueMap["name"] = legend
		valueMap["x"] = xResultList
		valueList = append(valueList, valueMap)
	}
	resultMap["legend"] = utils.Duplicate(legendList)
	resultMap["y"] = utils.Duplicate(yList)
	resultMap["data"] = valueList
	return &resultMap, nil
}
