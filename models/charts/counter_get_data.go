package charts

import (
	"data_view/core"
	"data_view/models"
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"
)

var CounterGetDataHandle = &models.ChartDataHandler{RunGetDataFromDB: func(db *sql.DB, chartDataParams *core.ChartDataParams) (string, error) {
	sqlString := chartDataParams.Sql
	rows, err := db.Query(sqlString)
	if err != nil {
		return core.EmptyString, err
	}
	defer rows.Close()
	dataResults, err := counterFormatRows(rows, chartDataParams)
	if err != nil {
		return core.EmptyString, err
	}
	resultString, err := json.Marshal(dataResults)
	if err != nil {
		return core.EmptyString, err
	}
	return string(resultString), nil
}, RunGetDataFromCsv: func(chartDataParams *core.ChartDataParams) (string, error) {
	return "", nil
}}

func counterFormatRows(rows *sql.Rows, chartDataParams *core.ChartDataParams) (*map[string]interface{}, error) {
	// 图表所需要的字段和数据库中字段的对应关系
	resultMap := make(map[string]interface{})
	dataField := chartDataParams.Data
	// 返回值列表
	dataResults := make([]map[string]interface{}, 0)
	columns, err := rows.Columns()
	if err != nil {
		return &resultMap, err
	}
	values := make([]sql.RawBytes, len(columns))
	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}
	for rows.Next() {
		if err := rows.Scan(scanArgs...); err != nil {
			return &resultMap, err
		}
		dataResult := make(map[string]interface{})
		var value string
		for i, col := range values {
			if col == nil {
				value = "NULL"
			} else {
				value = string(col)
			}
			if strings.EqualFold(columns[i], dataField) {
				dataResult["data"] = value
			}
		}
		dataResults = append(dataResults, dataResult)
	}
	fmt.Println(dataResults)
	if len(dataResults) > 0 {
		resultMap = dataResults[0]
		return &resultMap, nil
	} else {
		resultMap["data"] = 0
		return &resultMap, nil
	}
}
