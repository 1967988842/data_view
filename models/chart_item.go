package models

import (
	"data_view/core"
	"data_view/utils"
	"fmt"
)

type ChartItem struct {
	ItemId        uint64 `xorm:"pk autoincr notnull 'item_id'"`
	InstanceId    uint64 `xorm:"bigint(20) 'instance_id'"`
	ItemChartData string `xorm:"text 'item_chart_data'"`
	ItemChartType string `xorm:"varchar(20) 'item_chart_type'"`
	ItemChoose    string `xorm:"varchar(10) 'item_choose'"`
	ItemData      string `xorm:"text 'item_data'"`
	ItemHeight    uint64 `xorm:"bigint(20) 'item_height'"`
	ItemI         string `xorm:"varchar(20) 'item_i'"`
	ItemInterval  uint64 `xorm:"bigint(20) 'item_interval'"`
	ItemOption    string `xorm:"text 'item_option'"`
	ItemRefresh   string `xorm:"varchar(10) 'item_refresh'"`
	ItemWidth     uint64 `xorm:"bigint(20) 'item_width'"`
	ItemX         uint64 `xorm:"bigint(20) 'item_x'"`
	ItemY         uint64 `xorm:"bigint(20) 'item_y'"`
	ItemVersion   uint64 `xorm:"bigint(20) 'item_version'"`
}

func changeDataType(datas []map[string]string) []map[string]interface{} {
	cloneMaps := make([]map[string]interface{}, 0)
	for _, data := range datas {
		cloneMap := make(map[string]interface{})
		for key, value := range data {
			if key == "x" || key == "y" || key == "width" || key == "height" || key == "interval" {
				var newValue uint64
				_ = utils.StrToUint(value, &newValue)
				cloneMap[key] = newValue
			} else {
				cloneMap[key] = value
			}
		}
		cloneMaps = append(cloneMaps, cloneMap)
	}
	return cloneMaps
}

/**
 * 根据实例ID获取图表列表
 * @method GetChartItemByInstance、
 * @param [uint64] instanceId [实例ID]
 * @return [[]map[string]interface{}] [列表]
 * @return [error] [错误]
 */
//noinspection GoNilness
func GetChartItemByInstance(instanceId uint64, version uint64) (*[]map[string]interface{}, error) {
	var newDataResults []map[string]interface{}
	querySqlTemp := "select item_i as i, " +
		"item_x as `x`, " +
		"item_y as `y`, " +
		"item_width as `width`, " +
		"item_height as `height`, " +
		"item_chart_type as `chartType`, " +
		"item_choose as `choose`, " +
		"item_refresh as `refresh`, " +
		"item_chart_data as `chartData`, " +
		"item_data as `data`, " +
		"item_interval as `interval`, " +
		"item_option as `option` " +
		"from chart_item where instance_id = %d and item_version = %d order by REPLACE (item_i, 'chart', '') + 0"
	querySql := fmt.Sprintf(querySqlTemp, instanceId, version)
	dataResults, err := core.DB.SQL(querySql).QueryString()
	if err != nil {
		return &newDataResults, err
	}
	newDataResults = changeDataType(dataResults)
	return &newDataResults, nil
}

func SaveChartItem(chartItemList []map[string]interface{}, instanceId uint64, version uint64) error {
	for _, chartItemObject := range chartItemList {
		chartItem := new(ChartItem)
		chartItem.InstanceId = instanceId
		chartItem.ItemI = chartItemObject["i"].(string)
		chartItem.ItemX = uint64(chartItemObject["x"].(float64))
		chartItem.ItemY = uint64(chartItemObject["y"].(float64))
		chartItem.ItemWidth = uint64(chartItemObject["width"].(float64))
		chartItem.ItemHeight = uint64(chartItemObject["height"].(float64))
		chartItem.ItemChartType = chartItemObject["chartType"].(string)
		chartItem.ItemChoose = chartItemObject["choose"].(string)
		chartItem.ItemRefresh = chartItemObject["refresh"].(string)
		chartItem.ItemChartData = chartItemObject["chartData"].(string)
		chartItem.ItemData = chartItemObject["data"].(string)
		chartItem.ItemInterval = uint64(chartItemObject["interval"].(float64))
		chartItem.ItemOption = chartItemObject["option"].(string)
		chartItem.ItemVersion = version
		if _, err := core.DB.
			Insert(chartItem); err != nil {
			return err
		}
	}
	return nil
}
